
//Add list control with contet to unordered list view
function add_to_list(_idx, _content) {
     $("ul").append("<li id=\"index-" + _idx + "\">" + _content + "<button class=\"close del\">&times;</button> </li>");
}

//Is browser support local storage
function is_local_storage() {
    if (('localStorage' in window) && window['localStorage'] !== null) {
        return true;
    } else {
        return false;
    }
}

//Get current item count in web storage
function get_current_count() {
    if (is_local_storage()){
        var tmp_count = localStorage.getItem("count");
        if (typeof tmp_count != null) {
            var count = Number(tmp_count) % 1000;                           //circular list
        } else {
            var count = 0;
        }
    }
    return count;
}

//Load items in webstorage to unordered list view
function load_list_items() {
    var count = get_current_count();
    for (var idx = 0; idx < count; idx++) {
        var item = localStorage.getItem(idx);
        if (item != null) {
            add_to_list(idx, item); 
        }
    };
    if (count == 0) {
        $("#remove_button").hide();
    } else {
        $("#remove_button").show();
    }
}

//register add action
function register_add_item() {
    $("#mybutton").on('click', function() {
        var count = get_current_count();                                    //get count
        var content = $("#myinput").val();                                  //get content
        if (content.length > 0) {                                           //check content length
            content = "[" + moment().format('hh:mm:ss') + "]: " + content;  //add time stamp
            add_to_list(count, content);                                    //add to list
            if (is_local_storage()) {
                localStorage.setItem(count, content);                       //update content to webstorage
                count++;                                                    //count increase
                localStorage.setItem("count", count);                       //update count to webstorage
                $("#myinput").val("");                                      //clean input value
                $("#remove_button").show();
            }
         } else {
            console.log("empty string");
        }
    });
};

//register delete item
function register_delete_item() {
    $(document).on('click', ".del", function() {
        var idx = $(this).parent().attr('id').substring(6);                 //ignore index- pattern
        var target = $("#index-" + idx);                                    //target item in list view
        if (target) {
            $("#index-" + idx).remove();                                    //remove item from unordered list view
            if (is_local_storage()) {
                localStorage.removeItem(idx);                               //remove item from web storage
                if ($("li").size() == 0) {
                    localStorage.setItem("count", 0);                       //update count to webstorage
                    $("#remove_button").hide();
                }
            }
            
        } else {
            console.log("target not exit!");
        }
    })
}

//register remove all items
function register_remove_all_items() {
    $(document).on('click', "#remove_button", function() {
        localStorage.clear();                                               // remove local storage
        console.log("remove local storage");
        $('ul li').remove();
        $(this).hide();
    })
}

function register_effects() {
    $(document).on("mouseover", "li", function() {                          //for dynamic li hover effect
        $(this).fadeOut(100);
        $(this).fadeIn(200);
    });

    $("#myinput").keypress(function(event) {                                //bind enter key press on input
        if ( event.which == 13 ) {
            $("#mybutton").click();
        }
    });
}

//page ready
$(document).ready(function() {
    load_list_items();
    register_add_item();
    register_delete_item();
    register_remove_all_items();
    register_effects();
});

